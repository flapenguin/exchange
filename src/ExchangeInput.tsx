import Account from './Account';

import * as React from 'react';
import NumberFormat from 'react-number-format';
import ExchangeRow from './ExchangeRow';

import './ExchangeInput.css';

interface ExchangeInputProperties {
  accounts: Account[];
  account: Account;
  value: number;

  onChange(value: number): void;
  onAccountChange(account: Account): void;
}

const Input = (props: any[]) => <input className="ExchangeInput__input" {...props as any} />;

const ExchangeInput: React.StatelessComponent<ExchangeInputProperties> = props => (
  <ExchangeRow
    accounts={props.accounts}
    account={props.account}
    onAccountChange={props.onAccountChange}
  >
    <NumberFormat
      value={Math.abs(props.value) || ''}
      displayType="input"
      thousandSeparator={true}
      prefix="-"
      customInput={Input}
      onValueChange={(values: any) => {
        // Well, there's a bug in react-number-format.
        props.onChange(Math.abs(values.floatValue));
      }}
    />
  </ExchangeRow>
);

export default ExchangeInput;
