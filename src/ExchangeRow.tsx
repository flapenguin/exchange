import * as React from 'react';

import Account from './Account';

import './ExchangeRow.css';

type AnyChildren<T> = T | T[];

export interface ExchangeRowProperties {
  children: AnyChildren<string | JSX.Element>;
  info?: AnyChildren<string | JSX.Element>;

  accounts: Account[];
  account: Account;

  onAccountChange(account: Account): void;
}

const ExchangeRow: React.StatelessComponent<ExchangeRowProperties> = props => (
  <div className="ExchangeRow">
    <div className="ExchangeRow__top">
      <div className="ExchangeRow__currency">{props.account.currency.name}</div>
      <div className="ExchangeRow__value">{props.children}</div>
    </div>
    <div className="ExchangeRow__bottom">
      <div className="ExchangeRow__amount">
        You have
        {' '}
        {props.account.currency.sign}
        {props.account.amount.toFixed(2)}
      </div>
      <div className="ExchangeRow__info">
        {props.info}
      </div>
    </div>
    <ul className="ExchangeRow__accounts">
      {props.accounts.map(account =>
        <li key={account.currency.name}>
          <button
            title={account.currency.name}
            className={
              `ExchangeRow__account ${account === props.account ? 'ExchangeRow__account--selected' : ''}`}
            onClick={() => props.onAccountChange(account)}
          />
        </li>
      )}
    </ul>
  </div>
);

export default ExchangeRow;
