import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import appState from './appState';
import AppContainer from './AppContainer';

ReactDOM.render(
  <Provider store={appState}>
    <AppContainer />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
