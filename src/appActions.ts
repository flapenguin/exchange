export interface Exchange {
  readonly type: 'EXCHANGE';
}

export interface ChangeAmount {
  readonly type: 'CHANGE_AMOUNT';
  payload: number;
}

export interface ChangeSource {
  readonly type: 'CHANGE_SRC';
  payload: string;
}

export interface ChangeDestination {
  readonly type: 'CHANGE_DST';
  payload: string;
}

export interface UpdateRates {
  readonly type: 'UPDATE_RATES';
  payload: Record<string, number>;
}

export type Action =
  | Exchange
  | ChangeAmount
  | ChangeSource
  | ChangeDestination
  | UpdateRates;
