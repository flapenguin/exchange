export default interface Currency {
  name: string;
  sign: string;
}
