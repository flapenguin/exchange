import { createStore } from 'redux';
import reducer from './appReducer';
import Account from './Account';
import RatesUpdater from './RatesUpdater';

export function getExchangeRate(state: AppState) {
  return state.rates ? state.rates[`${state.exchange.src}:${state.exchange.dst}`] || 1 : null;
}

export type AppState = {
  rates: Readonly<Record<string, number>> | null;
  accounts: Readonly<Record<string, Account>>;
  exchange: {
    src: string;
    dst: string;
    amount: number;
  };
};

const reduxDevTools = (window as any).__REDUX_DEVTOOLS_EXTENSION__;
const store = createStore(reducer, reduxDevTools && reduxDevTools());
export default store;

// tslint:disable-next-line:no-unused-expression
new RatesUpdater(rates => store.dispatch(({ type: 'UPDATE_RATES', payload: rates })), {
  randomize: true,
  local: true,
  timeout: 10000,
  initialTimeout: 2000
});
