import { EUR, GBP, USD } from './currencies';
import { Action } from './appActions';
import { Reducer } from 'redux';

import { AppState, getExchangeRate } from './appState';

const initialAccounts = {
  USD: { currency: USD, amount: 1000 },
  GBP: { currency: GBP, amount: 600 },
  EUR: { currency: EUR, amount: 700 }
};

const initialState: AppState = {
  accounts: initialAccounts,
  rates: null,
  exchange: {
    src: 'USD',
    dst: 'EUR',
    amount: 0
  }
};

const reducer: Reducer<AppState> = (state = initialState, action: Action) => {
  switch (action.type) {
    default: break;
    case 'CHANGE_AMOUNT':
      state = { ...state, exchange: { ...state.exchange, amount: action.payload }};
      break;
    case 'CHANGE_SRC':
      state = {
        ...state,
        exchange: { ...state.exchange, src: action.payload }
      };
      break;
    case 'CHANGE_DST':
      state = {
        ...state,
        exchange: { ...state.exchange, dst: action.payload }
      };
      break;
    case 'UPDATE_RATES':
      state = {
        ...state,
        rates: action.payload
      };
      break;
    case 'EXCHANGE':
      const src = state.accounts[state.exchange.src];
      const dst = state.accounts[state.exchange.dst];
      const rate = getExchangeRate(state);

      if (!rate) {
        throw new Error('Rates were not loaded yet');
      }

      if (src.amount < state.exchange.amount) {
        throw new Error(`Not enough money on ${src.currency.name} to exchange ${state.exchange.amount}`);
      }

      state = {
        ...state,
        accounts: {
          ...state.accounts,
          [state.exchange.src]: {
            ...src,
            amount: src.amount - state.exchange.amount
          },
          [state.exchange.dst]: {
            ...dst,
            amount: dst.amount + state.exchange.amount * rate
          }
        },
        exchange: {
          ...state.exchange,
          amount: 0
        }
      };
      break;
  }

  return state;
};

export default reducer;
