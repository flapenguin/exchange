import * as React from 'react';
import ExchangeRow from './ExchangeRow';
import NumberFormat from 'react-number-format';

import Account from './Account';
import Currency from './Currency';

interface ExchangeOutputProperties {
  accounts: Account[];
  account: Account;
  sourceCurrency: Currency;

  value: number;
  rate: number;

  onAccountChange(account: Account): void;
}

const ExchangeOutput: React.StatelessComponent<ExchangeOutputProperties> = props => (
  <ExchangeRow
    info={`${props.sourceCurrency.sign}1 = ${props.account.currency.sign}${props.rate.toFixed(2)}`}
    account={props.account}
    accounts={props.accounts}
    onAccountChange={props.onAccountChange}
  >
    <NumberFormat
      value={props.value || ''}
      displayType="text"
      thousandSeparator={true}
      decimalScale={2}
      prefix="+"
      renderText={(value: string) => value}
    />
  </ExchangeRow>
);

export default ExchangeOutput;
