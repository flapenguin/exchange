import * as React from 'react';

import ExchangeInput from './ExchangeInput';
import ExchangeOutput from './ExchangeOutput';
import Account from './Account';

import './App.css';

export interface AppProps {
  accounts: Account[];
  srcAccount: Account;
  dstAccount: Account;
  amount: number;
  rate: number | null;

  exchange(): any;
  onAmountChange(amount: number): any;
  onSrcAccountChange(account: Account): any;
  onDstAccountChange(account: Account): any;
}

const App: React.StatelessComponent<AppProps> = props => (
  <div className={`App ${!props.rate ? 'App--disabled' : ''}`}>
    <div className="App__spend">
      <div className="App__actions">
        <button
          className="App__exchange"
          disabled={props.srcAccount === props.dstAccount || props.amount > props.srcAccount.amount}
          onClick={() => props.exchange()}
        >
          Exchange
        </button>
      </div>
      <ExchangeInput
        accounts={props.accounts}
        account={props.srcAccount}
        value={props.amount}
        onChange={value => props.onAmountChange(value)}
        onAccountChange={account => props.onSrcAccountChange(account)}
      />
    </div>
    <div className="App__gain">
      <div className="App__spacer" />
      <ExchangeOutput
        accounts={props.accounts}
        account={props.dstAccount}
        value={props.amount * (props.rate || 0)}
        sourceCurrency={props.srcAccount.currency}
        rate={props.rate || 0}
        onAccountChange={account => props.onDstAccountChange(account)}
      />
    </div>
  </div>
);

export default App;
