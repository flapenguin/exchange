import Currency from './Currency';

function makeCurrency(name: string, sign: string): Currency {
  return { name, sign };
}

export const GBP = makeCurrency('GBP', '£');
export const USD = makeCurrency('USD', '$');
export const EUR = makeCurrency('EUR', '€');
