export default function<V>(object: Record<string, V>): V[] {
  return Object.keys(object).reduce(
    (values, key) => {
      values.push(object[key]);
      return values;
    },
    [] as V[]);
}
