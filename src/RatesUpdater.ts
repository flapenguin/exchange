export default class RatesUpdater {
  private _fixerResponse: any = null;

  constructor(
    private onRatesUpdated: (rates: Record<string, number>) => any,
    private readonly options: {
      randomize?: boolean;
      local?: boolean;
      timeout?: number;
      initialTimeout?: number;
    }
  ) {
    options = {
      randomize: false,
      local: false,
      timeout: 10000,
      initialTimeout: 0,
      ...options
    };

    window.setTimeout(this._updateRates, options.initialTimeout);
  }

  private _randomise(value: number): number {
    return this.options.randomize ? value * (1 + Math.random() * 0.08 - 0.04) : value;
  }

  private async _request(): Promise<any> {
    if (this.options.local && this._fixerResponse) {
      // No need to call API constantly.
      return this._fixerResponse;
    }

    const request = await fetch('https://api.fixer.io/latest?base=USD');
    this._fixerResponse = await request.json();
    return this._fixerResponse;
  }

  private _updateRates = async () => {
    const data = await this._request();

    const rates: Record<string, number> = {};
    for (const key of ['EUR', 'GBP']) {
      rates[`USD:${key}`] = this._randomise(data.rates[key]);
      rates[`${key}:USD`] = 1 / rates[`USD:${key}`];
    }

    rates['EUR:GBP'] = this._randomise(rates['EUR:USD'] * rates['USD:GBP']);
    rates['GBP:EUR'] = 1 / rates['EUR:GBP'];

    this.onRatesUpdated(rates);

    window.setTimeout(this._updateRates, this.options.timeout || 10000);
  }
}
