import { AppState, getExchangeRate } from './appState';
import { Dispatch, bindActionCreators } from 'redux';

import App from './App';
import { connect } from 'react-redux';
import objectValues from './util/objectValues';
import Account from './Account';

const mapStateToProps = (state: AppState) => ({
  accounts: objectValues(state.accounts),
  srcAccount: state.accounts[state.exchange.src],
  dstAccount: state.accounts[state.exchange.dst],
  amount: state.exchange.amount,
  rate: getExchangeRate(state)
});

const mapDispatchToProps = (dispatch: Dispatch<AppState>) => bindActionCreators(
  {
    exchange: () => ({ type: 'EXCHANGE' }),
    onAmountChange: (amount: number) => ({ type: 'CHANGE_AMOUNT', payload: amount }),
    onSrcAccountChange: (account: Account) => ({ type: 'CHANGE_SRC', payload: account.currency.name }),
    onDstAccountChange: (account: Account) => ({ type: 'CHANGE_DST', payload: account.currency.name })
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(App);
