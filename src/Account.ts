import Currency from './Currency';

export default interface Account {
  amount: number;
  currency: Currency;
}
